//  
//  TabBarSceneModel.swift
//  Ms Fit
//
//  Created by Yura Granchenko on 22.02.2020.
//  Copyright © 2020 Selecto. All rights reserved.
//

import UIKit
import RealmSwift

enum TabBarSceneModel: String, CaseIterable {
    case daily, exercises, tips, profile
}
